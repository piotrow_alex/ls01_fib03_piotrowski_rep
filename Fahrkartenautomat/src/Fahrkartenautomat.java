import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        while (true) {
            //Fahrkarten Bestellung
            double zuZahlenderBetrag = fahrkartenbestellungErfassen();

            // Geldeinwurf
            double rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            // Fahrscheinausgabe
            fahrkartenAusgabe();

            // Rückgeldberechnung und -Ausgabe
            rueckgeldAusgabe(rueckgabeBetrag);

            System.out.println("""
                                    
                    Vergessen Sie nicht, den Fahrschein
                    vor Fahrtantritt entwerten zu lassen!
                    Wir wünschen Ihnen eine gute Fahrt. \n""");
        }
    }

    public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag = 0;

        double preis = 0;
        String[] fahrkarten = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
                "Kurzstecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] fahrkartenPreise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
        loop:
        while (true) {
            System.out.println("""
                    Fahrkartenbestellvorgang
                    ========================
                                        
                    Wählen sie bitte einen Fahrschein aus:
                    """);
            for (int i = 0; i < fahrkarten.length; i++) {
                System.out.printf("[%d] %s %.2f,\n",i+1,fahrkarten[i],fahrkartenPreise[i]);
            }
            System.out.print("Ihre Wahl: ");
            int wahl = tastatur.nextInt();
            if (wahl > fahrkarten.length){
                System.out.println("So eine Fahrkarte verkaufen wir nicht. Bitte wählen sie eine gültige Fahrkarte");
                continue;
            }
            preis = fahrkartenPreise[wahl-1];



            System.out.print("Anzahl der Tickets: ");
            double anzahlFahrscheine = tastatur.nextInt();
            while (anzahlFahrscheine < 1 || anzahlFahrscheine > 10) {
                System.out.println("Sie können nur zwischen, 1 und 10 Fahrscheinen kaufen.");
                System.out.print("Anzahl der Tickets: ");
                anzahlFahrscheine = tastatur.nextInt();

            }
            zuZahlenderBetrag += anzahlFahrscheine * preis;
            break;
        }

        return zuZahlenderBetrag;
    }

    /**
     * Methode lässt Benutzer fahrkarten Bezahlen und gibt den Rückgeldbetrag zurück
     *
     * @param zuZahlenderBetrag
     * @return double
     */
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgabe() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warten(250);
        }
        System.out.println("\n\n");
    }

    public static void warten(int millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void rueckgeldAusgabe(double rueckgabebetrag) {
        //auf nähesten 5 cent betrag runden weil java ungenau
        rueckgabebetrag = Math.round(rueckgabebetrag * 100.0 / 5.0) * 5.0 / 100.0;

        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            double rueckgabeBetragCent = (int) (rueckgabebetrag * 100);


            while (rueckgabeBetragCent >= 200) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "EURO");
                rueckgabeBetragCent -= 200;
            }
            while (rueckgabeBetragCent >= 100) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "EURO");
                rueckgabeBetragCent -= 100;
            }
            while (rueckgabeBetragCent >= 50) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "CENT");
                rueckgabeBetragCent -= 50;
            }
            while (rueckgabeBetragCent >= 20) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "CENT");
                rueckgabeBetragCent -= 20;
            }
            while (rueckgabeBetragCent >= 10) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "CENT");
                rueckgabeBetragCent -= 10;
            }
            while (rueckgabeBetragCent >= 5)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "CENT");
                rueckgabeBetragCent -= 5;
            }
        }

    }

    private static void muenzeAusgeben(int betrag, String einheit) {
        System.out.println(betrag + " " + einheit);
    }
}