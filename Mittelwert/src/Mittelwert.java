import java.util.Scanner;

public class Mittelwert {

    public static void main(String[] args) {

        // (E) "Eingabe"
        // Werte für x und y festlegen:
        // ===========================
        double x;
        double y;
        double m;

        Scanner tastatur = new Scanner(System.in);
        System.out.print("Bitte geben sie eine ganze Zahl ein: ");
        x = tastatur.nextDouble();

        System.out.print("Bitte geben sie eine zweite ganze Zahl ein: ");
        y = tastatur.nextDouble();


        // (V) Verarbeitung
        // Mittelwert von x und y berechnen:
        // ================================
        m = berechneMittelwert(x,y);

        // (A) Ausgabe
        // Ergebnis auf der Konsole ausgeben:
        // =================================
        System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);


    }

    public static double berechneMittelwert(double zahl1, double zahl2){
        return (zahl1 + zahl2) / 2.0;
    }
}